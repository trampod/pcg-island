using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using DataStructures.PriorityQueue;
using DG.Tweening;

public class IslandGenerator : MonoBehaviour
{
	[Header("Voronoi Generation")]
	public int MapSeed = 0;
    public Vector2Int MapSize;
	public float PointMinimalDistance;
	public float PointMaxDistance;

	[Header("Terain Generation")]
	public float MaxHeight;
	public float MinSeaLevel;
	public float SeaLevel;
	public int PeakCount;

	public float MinDrop;
	public float MaxDrop;
	public float UphillChance;
	public float UphillMax;

	[Header("Temperature Generation")]
	public float MaxTemperature;
	public float MinTemperature;
	public float MaxAverageTemperature;
	public float MinAverageTemperature;
	public float AverageTemperature;
	public float TemperaturePerUnit;

	[Header("Humidity Generation")]
	public float MaxHumidity;
	public float CoastLineHumidity;
	public float CloudHumidity;
	[Range(1,1000)]
	public int CloudCount;
	[Range(1f, 20f)]
	public float CloudHeight;
	public Vector3 WindDirection;

	const float CLOUD_DIRECTION_MAX_ANGLE_DIVIATION = 60;

	[Header("Biomes")]
	public Texture2D BiomeTexture;

	[Header("Debug")]
	public List<Vector2> Nodes = null;
	public List<Vector3> Vertexes = null;
	public List<Vector2>[] Edges = null;
	public List<Vector2>[] OrderedEdges = null;

	public MapNode[] mapNodes = null;
	public List<MapNode> borderNodes = null;

	public Material Material;

	public VoronoiVisualizer Visualizer;

	const int NUM_CENTROID_GEN_TRIES = 20;

	private bool ShowVisualizer = false;
	private bool ShowVertexes = false;
	private bool ShowEdges = false;
	private bool ShowCentroids = false;
	private bool ShowNeighbours = false;
	private bool ShowPolygons = true;
	private bool ShowWind = false;

	GameObject MapNodesGO;

	public enum MapColorStyle
    {
		Height,
		Humidity,
		Temperature,
		Biomes
    }

	public MapColorStyle ColorStyle = MapColorStyle.Biomes;

	private void Start()
	{
		Visualizer = FindObjectOfType<VoronoiVisualizer>();
		MapNodesGO = new GameObject();
		MapNodesGO.name = "Map Nodes";
		MapNodesGO.transform.parent = transform;
		GenerateIsland();
	}

    private void Update()
    {
		if(Visualizer.gameObject.activeInHierarchy != ShowVisualizer)
			Visualizer.gameObject.SetActive(ShowVisualizer);

		if(MapNodesGO.activeInHierarchy != ShowPolygons)
        {
			MapNodesGO.SetActive(ShowPolygons);
        }
	}

    void GenerateIsland()
    {
		ClearPolygons();

		Random.InitState(MapSeed);

        List<Vector2> centroids = GenerateCentroids();

		Nodes = centroids;

		int[,] bitmap = GenerateVoronoiBitmap(centroids);
        //CullBitmapSolidaries(ref bitmap); //was suposed to solve few cliping problems... in the end decided the cliping problems are the lesser evil for now


		//Create colored voronoi diagram for visualizations
        Visualizer.DrawDiagram(bitmap, centroids.Count);
        Visualizer.transform.position = new Vector3(0, SeaLevel, 0);

        Edges = GenerateEdges(bitmap,Nodes.Count);
		Edges = OrderEdges();

		GeneratePolygons(centroids,Edges);

		GenerateHeights(bitmap);
		SetSeaLevel();

		SetTemperature();
		SetHumidity();


		UpdateColor();
	}

	// was suposed to fix few clipping issues, but got suspended for now, since the problems were minor and this solution didn't quite work
    private void CullBitmapSolidaries(ref int[,] bitmap)
    {
		int width=bitmap.GetLength(0);
		int height=bitmap.GetLength(1);

        for(int x = 0; x < width; x++)
        {
			for(int y = 0; y < height; y++)
            {
				int cur = bitmap[x,y];
				if (x > 0 && cur == bitmap[x - 1, y])
					continue;
				if (x < width - 2 && cur == bitmap[x + 1, y])
					continue;
				if (y > 0 && cur == bitmap[x, y - 1])
					continue;
				if (y < height - 2 && cur == bitmap[x, y + 1])
					continue;

				if (x > 0)
                {
					bitmap[x,y] = bitmap[x - 1,y];
					continue;
                }
				if (y > 0)
				{
					bitmap[x, y] = bitmap[x, y - 1];
					continue;
				}
				if (x < width - 2)
				{
					bitmap[x, y] = bitmap[x + 1, y];
					continue;
				}
				if (y < height - 2)
				{
					bitmap[x, y] = bitmap[x, y + 1];
					continue;
				}
			}
        }
	}

	void ClearPolygons()
	{
		foreach (Transform child in MapNodesGO.transform)
		{
			GameObject.Destroy(child.gameObject);
		}
	}

	#region Voronoi
	// Generates centroids by trying to place new one near it
	List<Vector2> GenerateCentroids()
	{
		Vector3 initialPoint = new Vector3(
			Random.Range(0, MapSize.x),
			Random.Range(0, MapSize.y),
			0);

		List<Vector2> Points = new List<Vector2>();



		Points.Add(initialPoint);

		int expandedPoints = 0;

		while (expandedPoints < Points.Count)
		{
			Vector2 currentPoint = Points[expandedPoints];

			for (int i = 0; i < NUM_CENTROID_GEN_TRIES; i++)
			{
				Vector3 point = GenerateNewPoint(currentPoint);

				if (PointIsIn(point) && PointHasSpace(point, Points))
				{
					Points.Add(point);

					i = -1;
				}
			}

			expandedPoints++;
		}

		return Points;
	}

	// Generates new possible point near specified point
	private Vector3 GenerateNewPoint(Vector2 initialPoint)
	{
		Vector2 addition = GenerateNewPoint();
		return initialPoint + addition;
	}

	// Generates Relative new point
	private Vector2 GenerateNewPoint()
	{
		Vector3 newPoint = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), 0);

		newPoint.Normalize();

		float scale = Random.Range(PointMinimalDistance, PointMaxDistance);

		return newPoint * scale;
	}

	private bool PointIsIn(Vector2 point)
	{
		return point.x >= 0 && point.x < MapSize.x &&
			point.y >= 0 && point.y < MapSize.y;
	}

	// There is enough space around the point
	private bool PointHasSpace(in Vector2 point, in List<Vector2> Points)
	{
		foreach (Vector2 item in Points)
		{
			if (Vector2.Distance(item, point) < PointMinimalDistance)
			{
				return false;
			}
		}
		return true;
	}

	int[,] GenerateVoronoiBitmap(List<Vector2> centroids)
	{
		int[,] bitmap = new int[MapSize.x, MapSize.y];
		int width = bitmap.GetLength(0), height = bitmap.GetLength(1);

		//fill borders with dummy value (change so they are not needed
		//for (int x = 0; x < width; x++)
		//{
		//	bitmap[x, 0] = -1;
		//	bitmap[x, height-1] = -1;
		//}
		//for (int y = 0; y < height; y++)
		//{
		//	bitmap[0, y] = -1;
		//	bitmap[width-1, y] = -1;
		//}

		for (int x = 0; x < width; x++)
		{
			for (int y = 0; y < height; y++)
			{
				bitmap[x, y] = GetClosestCentroidIndex(new Vector2Int(x, y), centroids);
			}
		}

		return bitmap;
	}

	int GetClosestCentroidIndex(Vector2Int pixelPos, List<Vector2> centroids)
	{
		float smallestDst = float.MaxValue;
		int index = 0;
		for (int i = 0; i < centroids.Count; i++)
		{
			float dist = Vector2.Distance(pixelPos, centroids[i]);
			if (dist < smallestDst)
			{
				smallestDst = dist;
				index = i;
			}
		}
		return index;
	}
	#endregion

	#region Terrain
	// Creates hights for each cell based on the highest neighbour
	void GenerateHeights(int[,] bitmap)
	{
		List<MapNode> peaks = SetInitialHeights(bitmap);

		// Priority queue that goes from the highest to the lowest (Priority queue was not implemented by me)
		PriorityQueue<MapNode, float> nodes = new PriorityQueue<MapNode, float>(-MaxHeight);
		int queueCount = 0;

		foreach (MapNode peak in peaks)
		{
			nodes.Insert(peak, -peak.Height);
			queueCount++;
		}

		while (queueCount > 0)
		{
			MapNode node = nodes.Pop(), neighbour;
			queueCount--;
			for (int l = 0; l < node.Neighbours.Count; l++)
			{
				neighbour = node.Neighbours[l];
				if (neighbour.transform.position.y == 0)
				{
					if (Random.Range(0f, 1f) < UphillChance)
					{
						neighbour.Height = node.Height + Random.Range(0f, UphillMax);
					}
					else
					{
						neighbour.Height = node.Height - Random.Range(MinDrop, MaxDrop);
					}

                    neighbour.transform.position = new Vector3(
                        neighbour.transform.position.x,
                        neighbour.Height,
                        neighbour.transform.position.z);
                    nodes.Insert(neighbour, -neighbour.Height);
					queueCount++;
				}
			}
		}
	}

	// Creates multiple peaks
	List<MapNode> SetInitialHeights(int[,] bitmap)
	{
		List<MapNode> nodes = new List<MapNode>();
		int[] peaks = new int[PeakCount];
		int count = 0;
		while (count < peaks.Length)
		{
			int x = Random.Range(MapSize.x / 4, MapSize.x / 4 + MapSize.x / 2);
			int y = Random.Range(MapSize.y / 4, MapSize.y / 4 + MapSize.y / 2);

			bool present = false;
			for (int i = 0; i < count; i++)
			{
				if (peaks[i] == bitmap[x, y])
				{
					present = true;
					break;
				}
			}
			if (!present)
			{
				peaks[count++] = bitmap[x, y];
			}
		}

		for (int i = 0; i < peaks.Length; i++)
		{
			MapNode node = mapNodes[peaks[i]];
			node.Height = MaxHeight - MaxDrop * i;
			node.transform.position = new Vector3(node.transform.position.x, node.Height, node.transform.position.z);
			nodes.Add(node);
		}

		return nodes;
	}

	// Sets the sea level
	void SetSeaLevel()
	{
		SeaLevel = MinSeaLevel;
		// Rising the sea level so the border is always sea and the result is an actual island
		foreach (MapNode node in borderNodes)
		{
			SeaLevel = Mathf.Max(SeaLevel, node.transform.position.y);
		}

		foreach (MapNode node in mapNodes)
		{
			node.transform.position = new Vector3(
				node.transform.position.x,
				SeaLevel,
				node.transform.position.z);
			if (node.Height <= SeaLevel)
			{
				node.GetComponent<MeshRenderer>().material.color = Color.blue;
			}
			else
			{
				node.transform.DOMoveY(node.Height, 3f);
			}
		}
	}
	#endregion

	#region MeshGeneration
	//Creates meshes for each voronoi cell
	void GeneratePolygons(List<Vector2> centroids, List<Vector2>[] orderedEdges)
	{
		for (int i = 0; i < centroids.Count; i++)
		{
			if (orderedEdges[i].Count == 0) continue;

			GameObject go = mapNodes[i].gameObject;
			go.transform.parent = MapNodesGO.transform;
			go.transform.position = new Vector3(centroids[i].x, 0, centroids[i].y);
			Mesh mf = go.AddComponent<MeshFilter>().mesh;
			mf.Clear();

			int vertCount = orderedEdges[i].Count;
			Vector3[] verts = new Vector3[vertCount * 3];

			for (int l = 0; l < vertCount; l++)
			{
				verts[l] = new Vector3(orderedEdges[i][l].x, 0, orderedEdges[i][l].y) - go.transform.position;
				verts[vertCount + l] = verts[l]; // must be duplicit, so the top edges can be sharp sharp
				verts[vertCount * 2 + l] = new Vector3(orderedEdges[i][l].x, -10, orderedEdges[i][l].y) - go.transform.position;

			}
			mf.vertices = verts;
			int[] triangles = new int[(vertCount * 3 - 2) * 3];

			//top
			for (int l = 0; l < vertCount - 2; l++)
			{
				triangles[l * 3] = 0;
				triangles[l * 3 + 1] = l + 1;
				triangles[l * 3 + 2] = l + 2;
			}

			//sides
			for (int l = 0; l < vertCount; l++)
			{
				triangles[(vertCount - 2 + l) * 3] = l + vertCount;
				triangles[(vertCount - 2 + l) * 3 + 1] = l + vertCount * 2;
				triangles[(vertCount - 2 + l) * 3 + 2] = (l + 1) % vertCount + vertCount;
				triangles[(vertCount * 2 - 2 + l) * 3] = (l + 1) % vertCount + vertCount;
				triangles[(vertCount * 2 - 2 + l) * 3 + 1] = l + vertCount * 2;
				triangles[(vertCount * 2 - 2 + l) * 3 + 2] = ((l + 1) % vertCount) + vertCount * 2;
			}


			mf.triangles = triangles;
			mf.RecalculateBounds();

			mf.RecalculateNormals();
			MeshRenderer mr = go.AddComponent<MeshRenderer>();
			mr.material = Material;
		}
	}

	// Goes over the bitmap and creates vertices for creation of polygons
	List<Vector2>[] GenerateEdges(int[,] bitmap, int regionCount)
	{
		int[] a = new int[4]; // unique "colors"
		int count; // number of unique "colors"
		int[] i = new int[4]; // current square
		int width = bitmap.GetLength(0);
		int height = bitmap.GetLength(1);

		List<Vector2>[] vertexes = new List<Vector2>[regionCount];
		List<Vector3> vertexes2 = new List<Vector3>();
		mapNodes = new MapNode[regionCount];
		borderNodes = new List<MapNode>();

		for (int r = 0; r < regionCount; r++)
		{
			vertexes[r] = new List<Vector2>();

			GameObject go = new GameObject();
			mapNodes[r] = go.AddComponent<MapNode>();
			go.transform.parent = MapNodesGO.transform;
		}

		// the inside of the bitmap
		for (int x = 0; x < width - 1; x++)
		{
			for (int y = 0; y < height - 1; y++)
			{
				i[0] = bitmap[x, y];
				i[1] = bitmap[x + 1, y];
				i[2] = bitmap[x, y + 1];
				i[3] = bitmap[x + 1, y + 1];
				a[0] = i[0];
				a[1] = -1;
				a[2] = -1;
				a[3] = -1;
				count = 1;

				bool isThere;
				for (int n = 1; n < 4; n++)
				{
					isThere = false;
					for (int m = 0; m < count; m++)
					{
						if (a[m] == i[n])
						{
							isThere = true;
							break;
						}
					}

					if (!isThere)
					{
						a[count] = i[n];
						count++;
					}
				}

				// If more then two "colors" present, then there is vertex
				if (count > 2)
				{
					Vector2 v = GetVectorFromPixel(x, y);
					for (int n = 0; n < count; n++)
					{
						vertexes[a[n]].Add(v);
						for (int m = 0; m < count; m++)
						{
							if (n != m)
							{
								mapNodes[a[n]].AddNeighbour(mapNodes[a[m]]);
							}
						}
					}
					vertexes2.Add(new Vector3(v.x, v.y, 0));
				}
			}
		}

		//corners
		Vector2 w = GetVectorFromPixel(-1, -1);
		vertexes[bitmap[0, 0]].Add(w);
		vertexes2.Add(new Vector3(w.x, w.y, 0));
		borderNodes.Add(mapNodes[bitmap[0, 0]]);

		w = GetVectorFromPixel(-1, height - 1);
		vertexes[bitmap[0, height - 1]].Add(w);
		vertexes2.Add(new Vector3(w.x, w.y, 0));
		borderNodes.Add(mapNodes[bitmap[0, height - 1]]);

		w = GetVectorFromPixel(width - 1, -1);
		vertexes[bitmap[width - 1, 0]].Add(w);
		vertexes2.Add(new Vector3(w.x, w.y, 0));
		borderNodes.Add(mapNodes[bitmap[width - 1, 0]]);

		w = GetVectorFromPixel(width - 1, height - 1);
		vertexes[bitmap[width - 1, height - 1]].Add(w);
		vertexes2.Add(new Vector3(w.x, w.y, 0));
		borderNodes.Add(mapNodes[bitmap[width - 1, height - 1]]);

		// top and bottom border
		for (int x = 1; x < width; x++)
		{
			borderNodes.Add(mapNodes[bitmap[x, 0]]);
			borderNodes.Add(mapNodes[bitmap[x, height - 1]]);

			if (bitmap[x - 1, 0] != bitmap[x, 0])
			{
				mapNodes[bitmap[x - 1, 0]].AddNeighbour(mapNodes[bitmap[x, 0]]);
				mapNodes[bitmap[x, 0]].AddNeighbour(mapNodes[bitmap[x - 1, 0]]);

				w = GetVectorFromPixel(x - 1, -1);
				vertexes[bitmap[x, 0]].Add(w);
				vertexes[bitmap[x - 1, 0]].Add(w);
				vertexes2.Add(new Vector3(w.x, w.y, 0));
			}

			if (bitmap[x - 1, height - 1] != bitmap[x, height - 1])
			{
				mapNodes[bitmap[x - 1, height - 1]].AddNeighbour(mapNodes[bitmap[x, height - 1]]);
				mapNodes[bitmap[x, height - 1]].AddNeighbour(mapNodes[bitmap[x - 1, height - 1]]);

				w = GetVectorFromPixel(x - 1, height - 1);
				vertexes[bitmap[x, height - 1]].Add(w);
				vertexes[bitmap[x - 1, height - 1]].Add(w);
				vertexes2.Add(new Vector3(w.x, w.y, 0));
			}
		}

		// left and right border
		for (int y = 1; y < height; y++)
		{
			borderNodes.Add(mapNodes[bitmap[0, y]]);
			borderNodes.Add(mapNodes[bitmap[width - 1, y]]);

			if (bitmap[0, y - 1] != bitmap[0, y])
			{
				mapNodes[bitmap[0, y - 1]].AddNeighbour(mapNodes[bitmap[0, y]]);
				mapNodes[bitmap[0, y]].AddNeighbour(mapNodes[bitmap[0, y - 1]]);

				w = GetVectorFromPixel(-1, y - 1);
				vertexes[bitmap[0, y]].Add(w);
				vertexes[bitmap[0, y - 1]].Add(w);
				vertexes2.Add(new Vector3(w.x, w.y, 0));
			}

			if (bitmap[width - 1, y - 1] != bitmap[width - 1, y])
			{
				mapNodes[bitmap[width - 1, y - 1]].AddNeighbour(mapNodes[bitmap[width - 1, y]]);
				mapNodes[bitmap[width - 1, y]].AddNeighbour(mapNodes[bitmap[width - 1, y - 1]]);

				w = GetVectorFromPixel(width - 1, y - 1);
				vertexes[bitmap[width - 1, y]].Add(w);
				vertexes[bitmap[width - 1, y - 1]].Add(w);
				vertexes2.Add(new Vector3(w.x, w.y, 0));
			}
		}

		Vertexes = vertexes2;
		return vertexes;
	}

	// Creates vector from pixel, so it coresponds to the right place on Voronoi Visualiser
	private Vector2 GetVectorFromPixel(int x, int y)
	{
		return new Vector2(((float)x + 1f) - MapSize.x / 2, ((float)y + 1f) - MapSize.y / 2);
	}

	// help struct for ordering of vertices
	struct angle_order
	{
		public int integer;
		public float angle;
	}

	// Orderes all verticies so the edges can be used for polygons
	List<Vector2>[] OrderEdges()
	{
		List<Vector2>[] edges = new List<Vector2>[Edges.Length];
		for (int i = 0; i < 4; i++)
		{
			edges[i] = new List<Vector2>();
		}
		for (int i = 0; i < Edges.Length; i++)
		{
			edges[i] = OrderVertexes(Edges[i]);
		}
		return edges;
	}

	// Orderes Vertexes around their shared median
	List<Vector2> OrderVertexes(List<Vector2> list)
	{
		List<angle_order> indices = new List<angle_order>();
		Vector2 centroid = Vector2.zero;
		for (int i = 0; i < list.Count; i++)
		{
			centroid.x += list[i].x;
			centroid.y += list[i].y;
		}
		centroid.x /= list.Count;
		centroid.y /= list.Count;

		for (int i = 0; i < list.Count; i++)
		{
			indices.Add(new angle_order() { integer = i, angle = Vector2.SignedAngle(list[i] - centroid, Vector2.up) });
		}
		//indices = indices.OrderBy(o => o.angle).ToList();
		indices.Sort((o1, o2) => o1.angle.CompareTo(o2.angle));

		List<Vector2> result = new List<Vector2>();
		for (int i = 0; i < indices.Count; i++)
		{
			result.Add(list[indices[i].integer]);
		}
		return result;
	}
	#endregion

	#region Simulation
	void SetHumidity()
    {
		SetCoastlineHumidity();

		SimulateRain();
	}

	// Simulates number of clouds that can rain down on the island.
	void SimulateRain()
	{
		WindDirection = new Vector3(Random.Range(-1f, 1f), 0f, Random.Range(-1f, 1f));
		WindDirection.Normalize();

		int cloudsDone = 0;

		while (cloudsDone < CloudCount)
		{
			int i = Random.Range(0, borderNodes.Count);
			MapNode node = borderNodes[i];
			int moves = 0;
			List<MapNode> posibilities = new List<MapNode>();
			float angle;
			while (true)
			{
				posibilities.Clear();
				foreach (MapNode neighbour in node.Neighbours)
				{
					angle = Vector3.Angle(WindDirection, neighbour.transform.position - node.transform.position);
					if (angle <= CLOUD_DIRECTION_MAX_ANGLE_DIVIATION)
					{
						posibilities.Add(neighbour);
					}
				}
				if (posibilities.Count == 0)
				{
					break;
				}
				node = posibilities[Random.Range(0, posibilities.Count)];
				if (node.Height > SeaLevel)
				{
					float normalizedRainHeight = Mathf.Min(node.Height - SeaLevel, CloudHeight) / CloudHeight;
					if (Random.Range(0f, 1f) < normalizedRainHeight)
					{
						moves = 10;
						node.Humidity += CloudHumidity;
						if (node.Humidity > MaxHumidity)
							node.Humidity = MaxHumidity;
						foreach(MapNode neighbour in node.Neighbours)
						{
							neighbour.Humidity += CloudHumidity / node.Neighbours.Count;
							if (neighbour.Humidity > MaxHumidity)
								neighbour.Humidity = MaxHumidity;
						}
						break;
					}
				}
			}

			// this part is so that it resets clouds that lived too short lives, most likely because they spawned on the wrong side of border
			if (moves < 5)
			{
				continue;
			}
			cloudsDone++;
		}
	}

	// Sets some base humidity to every coastline cell
	void SetCoastlineHumidity()
    {
		foreach(MapNode node in mapNodes)
        {
			if(node.Height > SeaLevel)
            {
				int count = 0;
				foreach(MapNode neighbour in node.Neighbours)
                {
					if(neighbour.Height <= SeaLevel)
                    {
						// the more surounded by water, the more humid it is
						node.Humidity += CoastLineHumidity / (1 + count);
						count++;
                    }
                }
            }
        }
    }

	// Sets temperature based on random average temperature and height
    void SetTemperature()
	{
		AverageTemperature = Random.Range(MinAverageTemperature, MaxAverageTemperature);

		float min = float.MaxValue;
		float max = float.MinValue;
		float cur;

		foreach (MapNode node in mapNodes)
		{
			cur = node.Height;
			if (cur < min)
				min = cur;
			if (cur > max)
				max = cur;
		}

		foreach (MapNode node in mapNodes)
		{
			if (node.Height > SeaLevel)
			{
				cur = (node.Height - min) / max;
				node.Temperature = AverageTemperature - (node.Height - SeaLevel) * TemperaturePerUnit;
			}
		}
	}
    #endregion

    #region Coloring
    void UpdateColor()
    {
        switch(ColorStyle)
		{
			case MapColorStyle.Height:
				Debug.Log("UpdateColor - Height");
				ColorHeight();
				break;
			case MapColorStyle.Humidity:
				Debug.Log("UpdateColor - Humidity");
				ColorHumidity();
				break;
			case MapColorStyle.Temperature:
				Debug.Log("UpdateColor - Temperature");
				ColorTemperature();
				break;
			case MapColorStyle.Biomes:
				Debug.Log("UpdateColor - Biomes");
				ColorBiomes();
				break;
			default:
				break;
		}
    }

    void ColorHeight()
	{
		ColorStyle = MapColorStyle.Height;

		float min = float.MaxValue;
		float max = float.MinValue;
		float cur;

		foreach (MapNode node in mapNodes)
		{
			cur = Mathf.Max(node.Height,SeaLevel);
			if (cur < min)
				min = cur;
			if (cur > max)
				max = cur;
		}

		max -= min;

		foreach (MapNode node in mapNodes)
		{
			if (node.Height > SeaLevel)
			{
				cur = (node.Height - min) / max;
				node.GetComponent<MeshRenderer>().material.color = new Color(1f, 1f - cur, 0.6f * (1f - cur));
			}
		}
	}

	void ColorTemperature()
	{
		ColorStyle = MapColorStyle.Temperature;

		float max = MaxTemperature - MinTemperature;
		float cur;

		foreach (MapNode node in mapNodes)
		{
			if (node.Height > SeaLevel)
			{
				cur = (node.Temperature - MinTemperature) / max;
				node.GetComponent<MeshRenderer>().material.color = new Color(Mathf.Min(cur * (8f/5f) - 0.5f, 0.5f), 0f, Mathf.Max(0.5f - cur * (8f/3f), 0f));
			}
		}
	}

	void ColorHumidity()
	{
		ColorStyle = MapColorStyle.Humidity;

		float cur;

		foreach (MapNode node in mapNodes)
		{
			if (node.Height > SeaLevel)
			{
				cur = node.Humidity / MaxHumidity;
				node.GetComponent<MeshRenderer>().material.color = new Color(0, 0, cur);
			}
		}
	}

	void ColorBiomes()
	{
		ColorStyle = MapColorStyle.Biomes;

		foreach (MapNode node in mapNodes)
		{
			if(node.Height>SeaLevel)
            {
				node.GetComponent<MeshRenderer>().material.color = GetBiomeColor(node.Humidity, node.Temperature);
            }
		}
	}

	Color GetBiomeColor(float humidity, float temperature)
	{
		int hum = Mathf.Min((int)humidity, BiomeTexture.height - 1);
		//int hum = BiomeTexture.height - Mathf.Min((int)humidity, BiomeTexture.height - 1);
		int tem = (int)((temperature + 10) * 10);
		if (tem < 0)
			tem = 0;
		if (tem > BiomeTexture.width - 1)
			tem = BiomeTexture.width - 1;

		return BiomeTexture.GetPixel(tem, hum);
	}
	#endregion

	#region Display
	private void OnGUI()
	{
		if (GUILayout.Button("Generate Next"))
		{
			MapSeed++;
			GenerateIsland();
		}

		if (GUILayout.Button("Generate Same"))
		{
			GenerateIsland();
		}

		if (GUILayout.Button("Generate Previous"))
		{
			MapSeed--;
			GenerateIsland();
		}

		if (GUILayout.Button("Color Height"))
        {
			ColorHeight();
		}

		if (GUILayout.Button("Color Humidity"))
		{
			ColorHumidity();
		}

		if (GUILayout.Button("Color Temperature"))
		{
			ColorTemperature();
		}

		if (GUILayout.Button("Color Biomes"))
		{
			ColorBiomes();
		}

		ShowVisualizer = GUILayout.Toggle(ShowVisualizer, "Show Voronoi Visualizer");
		ShowVertexes = GUILayout.Toggle(ShowVertexes, "Show Voronoi Verticies");
		ShowEdges = GUILayout.Toggle(ShowEdges, "Show Voronoi Edges");
		ShowCentroids = GUILayout.Toggle(ShowCentroids, "Show Voronoi Centroids");
		ShowNeighbours = GUILayout.Toggle(ShowNeighbours, "Show Neighbour Connections");
		ShowPolygons = GUILayout.Toggle(ShowPolygons, "Show 3D Island");
		ShowWind = GUILayout.Toggle(ShowWind, "Show Wind Direction");
	}

	private void OnDrawGizmos()
	{
		if(WindDirection != null && ShowWind)
        {
			Gizmos.color = Color.white;
			Vector3 dir = WindDirection;
			dir *= 20;
			Gizmos.DrawSphere(new Vector3(0, 20, 0), 3f);
			Gizmos.DrawLine(new Vector3(0, 20, 0), new Vector3(dir.x, 20, dir.y));
        }

		if (Vertexes != null && ShowVertexes)
		{
			Gizmos.color = Color.yellow;
			foreach (Vector3 item in Vertexes)
			{
				Gizmos.DrawSphere(LayVector2to3(item), 0.5f);
			}
		}

		if(Nodes != null && ShowCentroids)
        {
			Gizmos.color = Color.red;
			foreach(Vector2 item in Nodes)
            {
				Gizmos.DrawSphere(LayVector2to3(item) - new Vector3(MapSize.x/2,0,MapSize.y/2), 0.5f);
            }
        }

		if (Edges != null && ShowEdges)
		{
			Gizmos.color = Color.green;
			foreach (List<Vector2> list in Edges)
			{
				for (int i = 0; i < list.Count; i++)
				{
					Gizmos.DrawLine(LayVector2to3(list[i]), LayVector2to3(list[(i + 1) % list.Count]));
				}
			}
		}

		if (mapNodes != null && ShowNeighbours)
		{
			Gizmos.color = Color.cyan;
			foreach(MapNode node in mapNodes)
			{
				foreach(MapNode neighbour in node.Neighbours)
				{
					Gizmos.DrawLine(WithY(node.transform.position,SeaLevel) - new Vector3(MapSize.x/2, 0, MapSize.y/2), WithY(neighbour.transform.position,SeaLevel) - new Vector3(MapSize.x/2, 0, MapSize.y/2));
				}
			}
		}
	}

	Vector3 WithY(Vector3 vec, float Y)
    {
		return new Vector3(vec.x, Y, vec.z);
    }

	Vector3 LayVector2to3(Vector2 vec)
    {
		return new Vector3(vec.x, SeaLevel, vec.y);
    }
    #endregion
}
