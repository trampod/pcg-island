using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapNode : MonoBehaviour
{
	public float Height;
	public float Temperature;
	public float Humidity;

	public List<MapNode> Neighbours;

	public void AddNeighbour(MapNode node)
	{
		if(Neighbours == null)
		{
			Neighbours = new List<MapNode>() { node };
		}
		else if (!Neighbours.Contains(node))
		{
			Neighbours.Add(node);
		}
	}
	
	public void SetHeightColor()
    {

    }

	public void SetTemperatureColor()
    {

    }

	public void SetHumidityColor()
    {

    }

	public void SetBiomColor()
    {

    }
}
