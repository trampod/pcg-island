using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VoronoiVisualizer : MonoBehaviour
{
	public Vector2Int imageDim;
	public int regionAmount;

	Color[] regions = null;


	private void Start()
	{
	}

	public void DrawDiagram(List<Vector2> centroids, Vector2Int dimentions)
	{
		imageDim = dimentions;
		regionAmount = centroids.Count;
		GetComponent<SpriteRenderer>().sprite = Sprite.Create(GetDiagram(centroids), new Rect(0, 0, imageDim.x, imageDim.y), Vector2.one * 0.5f,1);
	}

	public void DrawDiagram(int[,] bitmap, int regionCount)
	{
		imageDim = new Vector2Int(bitmap.GetLength(0),bitmap.GetLength(1));
		regionAmount = regionCount;
		GetComponent<SpriteRenderer>().sprite = Sprite.Create(GetDiagram(bitmap), new Rect(0, 0, imageDim.x, imageDim.y), Vector2.one * 0.5f,1);
	}

	Texture2D GetDiagram(int[,] bitmap)
	{
		regions = new Color[regionAmount];
		for (int i = 0; i < regionAmount; i++)
		{
			regions[i] = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), 1f);
		}


		Color[] pixelColors = new Color[imageDim.x * imageDim.y];
		for (int x = 0; x < imageDim.x; x++)
		{
			for (int y = 0; y < imageDim.y; y++)
			{
				int index = x * imageDim.x + y;
				pixelColors[index] = regions[bitmap[x,y]];
			}
		}
		return GetImageFromColorArray(pixelColors);
	}

	Texture2D GetDiagram(List<Vector2> centroids)
	{
		regions = new Color[regionAmount];
		for (int i = 0; i < regionAmount; i++)
		{
			regions[i] = new Color(Random.Range(0f, 1f), Random.Range(0f, 1f), Random.Range(0f, 1f), 1f);
		}


		Color[] pixelColors = new Color[imageDim.x * imageDim.y];
		for (int x = 0; x < imageDim.x; x++)
		{
			for (int y = 0; y < imageDim.y; y++)
			{
				int index = x * imageDim.x + y;
				int regionIndex = GetClosestCentroidIndex(new Vector2Int(x, y), centroids);
				pixelColors[index] = regions[regionIndex];
			}
		}
		return GetImageFromColorArray(pixelColors);
	}

	int GetClosestCentroidIndex(Vector2Int pixelPos, List<Vector2> centroids)
	{
		float smallestDst = float.MaxValue;
		int index = 0;
		for (int i = 0; i < centroids.Count; i++)
		{
			float dist = Vector2.Distance(pixelPos, centroids[i]);
			if (dist < smallestDst)
			{
				smallestDst = dist;
				index = i;
			}
		}
		return index;
	}
	Texture2D GetImageFromColorArray(Color[] pixelColors)
	{
		Texture2D tex = new Texture2D(imageDim.x, imageDim.y);
		tex.filterMode = FilterMode.Point;
		tex.SetPixels(pixelColors);
		tex.Apply();
		return tex;
	}
}
