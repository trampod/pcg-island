using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class IslandCamera : MonoBehaviour
{
    public IslandGenerator IslandMap;

    public float RotationSpeed;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = new Vector3(transform.position.x, IslandMap.SeaLevel, transform.position.z);
        transform.Rotate(0, RotationSpeed * Time.deltaTime, 0);
    }
}
