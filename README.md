# Proceduraly Generated Islands

This project is about proceduraly generated islands based on simplified simulations.

## Voronoi Diagram

Basis for the terrain are the voronoi diagrams. These are created as simple bitmaps with low resolution for faster calculation.

<img src="Images/VoronoiClose.PNG" alt="Colored cells based on vertices" width="300"/>

Based on this bitmap are then chosen "sharp" corners of the voronoi cells.

<img src="Images/VoronoiVerticies.PNG" alt="Vertices where multiple cells meet" width="300"/>

These are then connected with edges to create polygons.

<img src="Images/VoronoiEdges.PNG" alt="Vertices connected with edges" width="300"/>

These polygons can be then used to generate 3D objects representing each cell in form of top polygon and then extrusion downward.

<img src="Images/VoronoiOnlyEdges.PNG" alt="Vertices connected with edges" width="300"/>

### Height

Height of each cell is desided by first setting multiple peaks (in examples this is 3, but it can be set to any number) with different heights (1. has max height and each subsequent is bit lower).

The rest is decided by making the neighbours bit lower until every cell has set height. The amount by which it is lowered is random in certain interval. There is also small chance to make the neighbour a bit higher to the shape is not too much cone-shaped.

After the heights are done the sea level is set. This is either predefined height, or the heighest border cell. This is so that the border is always on the sea level and the result is island every time.

<img src="Images/VoronoiHeight.PNG" alt="Voronoi height map" width="300"/>

### Temperature

Temperature is set in very simple way. Since the island is small, random base temperature for the whole island is decided. Each cell has then it's specific temperature based on this base temperature and it's height.

<img src="Images/MapTemperature.PNG" alt="Voronoi temperature map" width="300"/>

### Humidity

Humidity is set in two ways. Base coastline humidity and simple rain simulation.

Each cell on the coastline gets some small humidity, because it's adjecent to the sea. The amount is also scaled with the amount of neighnouring sea cells, but every additional cell gives diminishing bonus to humidity.

Rain is simulated by creating specified number of clouds and deciding a random direction of wind. Each cloud is moved to random neighbour in the direction of the wind. If the clouds is above terrain it has chance to rain it's water on current cell and on it's neighbours (although in smaller amount). The chance of rain is also affected by the height of the cell. The higher the cell, the higher the chance of rain, so the clouds are "stoped" by the island and mostly rain one side.

<img src="Images/MapHumidity.PNG" alt="Voronoi humidity map" width="300"/>

### Biomes

Biomes are represented only by color. And they are decided based on the humidity and temperature of the cell. These two values are taken and using biome texture is gained a coresponding color from it.

<img src="Assets/Biome Map 4.png" alt="Biome texture" width="200"/>

This texture is made very discretely, since the island itself consists of discrete cells. The color is then aplied on the entire cell.

<img src="Images/MapBiomes.PNG" alt="Voronoi biome map" width="300"/>

### Final Product

Final product is 3D representation of the island.

<img src="Images/Island.PNG" alt="3D Island" width="300"/>

## Planned Improvements

### Optimalization

Make the generation faster. Mainly by creating structure for centroids and MapNodes. Most likely by organising them insto grids and then using only those cells of the grid that are relevant.

### Better Biome Map

Although the current biome map is fine, there still could be come improvements. Also due to simplicity of the biome map, there could be multiple different maps that could change diferent types of planets or planes of existence for example.

### Aditional Generation

Generating more aspects like for example:

- Rivers
	- Running alongside the sides of cells. Always going down and filling local minimums with water.
- Towns
	- Settlements based on the living conditions (temperature, humidity, source of water)
- Points of interest
	- More specificaly locations of dungeons. Most likely in higher places, further away from civilization.
